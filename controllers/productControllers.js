const Product = require('./../models/Products')

module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})
	
	return newProduct.save().then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}

module.exports.allProduct = () => {
	return Product.find().then( result => {
		return result
	})
}

module.exports.getSingleProduct = (params) => {
	
	return Product.findById(params.productId).then( product => {

		return product
	})
}

module.exports.editProduct = (params, reqBody) => {
	
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(params, updatedProduct, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

module.exports.archiveProduct = (params) => {

	let archiveProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params, archiveProduct, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}

module.exports.unarchiveProduct = (params) => {

	let unarchiveProduct = {
		isActive: true
	}

	return Product.findByIdAndUpdate(params, unarchiveProduct, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}





