const express = require("express");
const router = express.Router();

const userController = require("./../controllers/userControllers");

const auth = require('./../auth');

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

router.post("/register", (req, res)=> {
	userController.register(req.body).then(result => res.send(result));
})

router.post("/login", (req, res) => {

	userController.login(req.body).then( result => res.send(result))
})

router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {

	userController.setAsAdmin(req.params.userId).then( result => res.send(result))
})

router.put('/:userId/removeAsAdmin', auth.verify, (req, res) => {

	userController.removeAsAdmin(req.params.userId).then( result => res.send(result))
})

router.post('/checkout', auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		totalAmount: req.body.totalAmount
	}

	userController.checkout(data).then( result => res.send(result))
})

router.get("/orders", auth.verify, (req, res)=>{
	userController.allOrders().then(result => res.send(result));
});

router.get("/:userId/myOrders", auth.verify, (req, res)=>{
	let params = req.params.userId

	userController.myOrders(params).then( result => res.send(result));
})



module.exports = router;

